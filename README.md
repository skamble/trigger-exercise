# Trigger exercise for the PO & DAS 2023 in Hamburg 

This repository contains the DPG Trigger exercise of the Physics Object & Data Analysis School held in Hamburg in October 2023: https://indico.desy.de/event/38207/

The focus of this exercise is to learn the basics of the trigger usage and efficiency measurements using the CMS `NanoAOD` data tier. As an example a MET and a single muon trigger are used. The exercise uses so-called columnar data analysis tools such as [`awkward`](https://awkward-array.org/doc/main/index.html), [`vector`](https://pypi.org/project/vector/) and [`uproot`](https://uproot.readthedocs.io/en/latest/) for [`ROOT`](https://root.cern/)-less I/O.

Previous trigger exercises which also feature the direct usage of the more low-level `MiniAOD` data tier can be found here:
https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideCMSDataAnalysisSchoolCERN2023TriggerExercise 

## Prerequisites / environment

This exercise is meant to be run in [Jupyter Noteboks](https://jupyter.org/) and recommended using the [CERN-own jupyter server SWAN](https://swan.cern.ch/). 

### Using SWAN

See the SWAN documention to get started: [https://swan.docs.cern.ch/intro/what_is/](https://swan.docs.cern.ch/intro/what_is/)
Essentially one needs a CERN computing account and access to [`CERNbox`](https://cernbox.cern.ch/) to store data in the `EOS` file system.

#### Clone the repository to your SWAN/CERNbox area:

If you are familiar with lxplus, you can log in to it and `cd` to your `EOS` home area and clone the project:
```bash
ssh username@lxplus.cern.ch
cd my/eos/path/SWAN_projects/
git clone https://gitlab.cern.ch/cms-podas23/dpg/trigger-exercise.git
```

Otherwise you can do the same via the web broswer:
0. Make sure you have a CERNbox area by going to [https://cernbox.cern.ch/](https://cernbox.cern.ch/)
1. Log in to SWAN via the webbrowser: [swan.cern.ch](https://swan.cern.ch/) and launch the server
2. On the "My Projects" page on the right click on the cloud icon and enter the git URL: https://gitlab.cern.ch/cms-podas23/dpg/trigger-exercise.git
3. Go to the newly created `trigger-exercise` porject folder and start with the notebook `1_Intro...`

You will be able to find this folder in your `/eos/home-u/username/SWAN_projects/trigger-exercise` folder via lxplus.

### DESY Jupyter hub

Login to naf with your username:
```
ssh username@naf-cms.desy.de
```

Then execute the following commands to install a local jupyter kernel that we will use forthgoing:

``` 
source /nfs/dust/cms/user/bachjoer/podas_env/bin/activate 
python -m ipykernel install --user --name=podas
```

Finally, clone this repository into your home directory (or wherever you want):
```
git clone https://gitlab.cern.ch/cms-podas23/dpg/trigger-exercise.git
```

You can then go to https://naf-jhub.desy.de/ and login to get a Jupyter notebook. Use the `podas` kernel after opening the notebooks!

See the DESY Jhub/NAF documentation: https://confluence.desy.de/display/IS/Jupyter+on+NAF
