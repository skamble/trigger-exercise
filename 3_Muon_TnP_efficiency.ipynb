{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "95a0a93b",
   "metadata": {},
   "source": [
    "# Muon trigger efficiency measurement using Tag-and-probe\n",
    "\n",
    "The goal of this exercise is to measure the efficiency of a muon HLT path e.g. `IsoMu24` using the so-called \"tag-and-probe\" method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5670e08d",
   "metadata": {},
   "outputs": [],
   "source": [
    "## basic librarires\n",
    "from glob import glob\n",
    "import numpy as np\n",
    "\n",
    "## for reading ROOT files and the analysis\n",
    "import uproot\n",
    "import awkward as ak\n",
    "import vector\n",
    "vector.register_awkward()\n",
    "\n",
    "## for plotting\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib.colors import LogNorm\n",
    "%matplotlib inline\n",
    "\n",
    "# HEP-stype plots\n",
    "## uncomment when using the DESY Hub !!!\n",
    "import mplhep as hep\n",
    "hep.style.use(\"CMS\")\n",
    "plt.rcParams['figure.figsize'] = (8,6)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21464859",
   "metadata": {},
   "source": [
    "# Loading data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "659fee55",
   "metadata": {},
   "source": [
    "As we are interested in the events triggered by a Muon HLT path we again can use the `Muon` dataset file we used in the previous exercises.\n",
    "\n",
    "* CERN/EOS location: `/eos/home-a/alobanov/CMS/PODAS23/dpg_trig_ex/short-ex-trg/ex2/Muon2022F_NanoAOD/Muon_Run2022F_PromptNanoAODv11_cd8d1b0c-5ade-4591-beb8-93034cc7ca11.root`\n",
    "* DESY location: `/nfs/dust/cms/group/cmsdas2023/DPG-Trigger-Ex/short-ex-trg/ex2/Muon2022F_NanoAOD/Muon_Run2022F_PromptNanoAODv11_cd8d1b0c-5ade-4591-beb8-93034cc7ca11.root`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9477388",
   "metadata": {},
   "outputs": [],
   "source": [
    "files=[\"/eos/home-a/alobanov/CMS/PODAS23/dpg_trig_ex/short-ex-trg/ex2/Muon2022F_NanoAOD/Muon_Run2022F_PromptNanoAODv11_cd8d1b0c-5ade-4591-beb8-93034cc7ca11.root\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93ce2be4",
   "metadata": {},
   "source": [
    "We will load the same branches as before, i.e. the `Muon` and `HLT_IsoMu24`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8fec1b3e",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "branches = [\"/(Muon)_(pt|eta|phi|tightId|pfRelIso03_all)/\"]\n",
    "branches += [\"HLT_IsoMu24\"]\n",
    "\n",
    "events = uproot.concatenate(\n",
    "    {fname:\"Events\" for fname in files},\n",
    "    filter_name = branches,\n",
    "    how = \"zip\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd0383e0",
   "metadata": {},
   "source": [
    "The array events contains now the `Muon`s and HLT decision:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f90105c2",
   "metadata": {},
   "outputs": [],
   "source": [
    "events.fields"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a8bad63",
   "metadata": {},
   "source": [
    "Let's assign the muons to a separate array `muons` for convenience (to avoid to always type events):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec48e0d3",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "muons = events.Muon\n",
    "muons.fields"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "738f67de",
   "metadata": {},
   "source": [
    "We will add two variables which we need to use later: \n",
    "* the index of the muon in the event using `ak.local_index`\n",
    "* the mass of a muon: 0.105 GeV"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12624c6a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# add index as variable\n",
    "muons[\"idx\"] = ak.local_index(muons)\n",
    "\n",
    "# add mass\n",
    "muons[\"mass\"] = 0.105"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c2c25ff",
   "metadata": {},
   "source": [
    "## Trigger Objects\n",
    "\n",
    "**A novely in this exercise will be the use of the \"Trigger Objects\" information**: \n",
    "these contain the information about the actual HLT objects that fired the triggers, e.g. the HLT-muons that fired the `IsoMu24` path.\n",
    "\n",
    "You can find some more info in the NanoAOD twiki: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD#Trigger.\n",
    "A detailed description (more human readable is here in the [`NanoAOD` auto-description](https://cms-nanoaod-integration.web.cern.ch/autoDoc/NanoAODv12/2023Prompt/doc_EGamma0_Run2023C-PromptNanoAODv12_v2-v2.html))\n",
    "\n",
    "We will discuss the use of these objects below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2bdc2401",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "trig_objs = uproot.concatenate(\n",
    "    {fname:\"Events\" for fname in files},\n",
    "    filter_name = \"/(TrigObj)_(pt|eta|phi|id|filterBits)/\",\n",
    "    how = \"zip\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7cb3a48f",
   "metadata": {},
   "outputs": [],
   "source": [
    "trig_objs = trig_objs[\"TrigObj\"]\n",
    "trig_objs.fields"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbe78c5c",
   "metadata": {},
   "source": [
    "# The Tag-and-probe method"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84091498",
   "metadata": {},
   "source": [
    "We will need this trigger object information in order to match our \"offline\" reconstructed `Muon` to these HLT trigger objects in order to compute the efficiency.\n",
    "The concept is: if an HLT muon can be matched to an offline muon then we say that the trigger is efficient. If we cannot find an offline muon corresponding to the HLT muon, the HLT muon must have been mis-resconstructed and thus the trigger is inefficient.\n",
    "\n",
    "With the tag-and-probe method we do the following:\n",
    "* We find offline muons satisfying \"good Muon\" criteria\n",
    "* `Tag` muons are defined as good muons matching to a trigger object -> we ensure that the event was triggered due to this muon\n",
    "* `Probe` muons are good muons, which together with the tag muon form a \"Z Candidate\", i.e. the invariant di-Muon mass `(Tag+Probe).mass` should be close to the Z mass peak -> we ensure that the probe muon is likely real muon\n",
    "* We measure the \"trigger efficiency\" as the efficiency of an offline probe muon to be matched to an online/HLT muon"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e44e45b",
   "metadata": {},
   "source": [
    "The Tag muon criteria are identical to what we used in the previous exercise:\n",
    "```\n",
    "# Tag muon requirements: pT > 28 GeV, |eta|<2.4, tight-ID\n",
    "if tag_muons.pt < 28: continue\n",
    "if abs(tag_muons.eta) > 2.4: continue\n",
    "if not tag_muons.tightId: continue\n",
    "if tag_muons.pfRelIso03_all > 0.15: continue\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f30b383c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# define good muons\n",
    "\n",
    "good_mu_mask = (abs(muons.eta) <= 2.4) & (muons.pt >= 20)\n",
    "good_mu_mask = good_mu_mask & muons.tightId\n",
    "good_mu_mask = good_mu_mask & (muons.pfRelIso03_all <= 0.15)\n",
    "\n",
    "good_muons = muons[good_mu_mask]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f227db9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# define tag muons\n",
    "\n",
    "tag_mu_mask = (abs(muons.eta) <= 2.4) & (muons.pt >= 28)\n",
    "tag_mu_mask = tag_mu_mask & muons.tightId\n",
    "tag_mu_mask = tag_mu_mask & (muons.pfRelIso03_all <= 0.15)\n",
    "\n",
    "tag_muons = muons[tag_mu_mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e794f7c",
   "metadata": {},
   "source": [
    "Probe muons have identical requirements except that the pT threshold is lowered to 20 GeV since we want to see the \"turn-on\" of the trigger efficiency (Q: where should that turn-on point be?)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "da3bd021",
   "metadata": {},
   "outputs": [],
   "source": [
    "# define probe muons\n",
    "\n",
    "probe_mu_mask = (abs(muons.eta) <= 2.4) & (muons.pt >= 20)\n",
    "probe_mu_mask = probe_mu_mask & muons.tightId\n",
    "probe_mu_mask = probe_mu_mask & (muons.pfRelIso03_all <= 0.15)\n",
    "\n",
    "probe_muons = muons[probe_mu_mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14bc13e7",
   "metadata": {},
   "source": [
    "Now comes a tricky part: we want to select Trigger Objects `TrigObj` that correspond to our HLT path of interest: `IsoMu24`.\n",
    "\n",
    "For this we first will use the `TrigObj_filterBits` branch, for which in the above linked documentation of NanoAOD one can read:\n",
    "> `1 => Iso, 2 => OverlapFilter PFTau, 3 => 1mu, 4 => 2mu, 5 => 1mu-1e, 6 => 1mu-1tau, 7 => 3mu, 8 => 2mu-1e, 9 => 1mu-2e, 10 => 1mu (Mu50), 11 => 1mu (Mu100), 12 => 1mu-1photon for Muon`\n",
    "\n",
    "We have single Muon trigger --> we use need to require that the bit `3` of `filterBits` is \"enabled\".\n",
    "We will do that like this: `(trig_objs.filterBits & (1<<3)) > 0)`\n",
    "\n",
    "Then, we actually want to use the Muons from this object and for this we need to select objects with `TrigObj_id==13`, as explained in the documentation:\n",
    "> `TrigObj_id\tUShort_t\tID of the object: 1515 = BoostedTau, 11 = Electron(PixelMatched e/gamma), 6 = FatJet, 3 = HT, 1 = Jet, 2 = MET, 4 = MHT, 13 = Muon, 22 = Photon, 15 = Tau`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "121dbd97",
   "metadata": {},
   "outputs": [],
   "source": [
    "# filter trig objects by id to get the Muons\n",
    "trgobj_mask = trig_objs.id == 13\n",
    "# filter trig objects by filterBits to get the SingleMu\n",
    "# NB this operation uses the bit-shift operator << \n",
    "trgobj_mask = trgobj_mask & ((trig_objs.filterBits & (1<<3)) > 0)\n",
    "\n",
    "mu_trg_obj = trig_objs[trgobj_mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c850fc0",
   "metadata": {},
   "source": [
    "### Task: plot the per event number of muon trigger objects as we have selected above (`mu_trg_obj`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dca275d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = plt.hist(ak.num(mu_trg_obj), bins = range(5), log = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8868ce39",
   "metadata": {},
   "source": [
    "## Match the good muons to the Trigger Object/HLT Muons"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b38a4d91",
   "metadata": {},
   "source": [
    "We will be using the [`vector` package](https://pypi.org/project/vector/) in order to get access to 4-vector operations such as `deltaR` or the invariant mass.\n",
    "\n",
    "For this an array should contain at least these branches: `pt`, `eta`, `phi`, `mass` (remember we have added the `mass` to the muons?)\n",
    "\n",
    "Then we have to do the following to make the arrays `vector`s:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f469924c",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_muons = ak.Array(good_muons, with_name = \"Momentum4D\")\n",
    "mu_trg_obj = ak.Array(mu_trg_obj, with_name = \"Momentum3D\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce2239e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# tag_muons = ak.Array(tag_muons, with_name = \"Momentum4D\")\n",
    "# probe_muons = ak.Array(probe_muons, with_name = \"Momentum4D\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d030265",
   "metadata": {},
   "source": [
    "### deltaR matching using combinatorics\n",
    "\n",
    "In order to do the \"dR-matching\" (dR is the distance in the eta/phi plane between two 4-vectors) we need to build combinations of e.g. good muons and trigger objects.\n",
    "We will do this with the `ak.cartesian` function as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9642dd17",
   "metadata": {},
   "outputs": [],
   "source": [
    "offmu_trgobj_combo = ak.cartesian({\"offl\": good_muons,\"trgobj\": mu_trg_obj}, nested=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9dc22a7b",
   "metadata": {},
   "source": [
    "Note the result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74d951bf",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "offmu_trgobj_combo.offl.pt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88f2e1c4",
   "metadata": {},
   "source": [
    "And compare to the \"original\" `good_muons` array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "047a32a1",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_muons.pt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0925d0e3",
   "metadata": {},
   "source": [
    "As you can see, the `combo` array adds another \"layer\" of nesting: this corresponds to the added layer of combinations, e.g. per event you now have MxN entries where M is the number of good muons (yellow) and N the number of trigobjs (pink).\n",
    "![](https://raw.githubusercontent.com/jpivarski-talks/2020-07-13-pyhep2020-tutorial/master/img/cartoon-cartesian.png)\n",
    "\n",
    "The graphic is taken from this tutorial which you can use as reference: https://github.com/jpivarski-talks/2020-07-13-pyhep2020-tutorial/tree/master"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57729e66",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Now we can \"unzip\" the combinations to go back to each array:\n",
    "offmu,trgobj = ak.unzip(offmu_trgobj_combo)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "decb5b97",
   "metadata": {},
   "source": [
    "Now we can compute the `deltaR` between the `offmu` and `trgobj` arrays:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e31c2a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "dR = offmu.deltaR(trgobj) #magic?!\n",
    "dR"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02ffeb6d",
   "metadata": {},
   "source": [
    "Now we want to make a histogram of this variable and we need to flatten the dR array to a simple 1D array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15d50b20",
   "metadata": {},
   "outputs": [],
   "source": [
    "ak.flatten(dR)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "683f5819",
   "metadata": {},
   "source": [
    "But oh no, you see we still have a layer of nesting? \n",
    "We can use `ak.ravel` to make sure there is no more nesting! "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "933c4ede",
   "metadata": {},
   "outputs": [],
   "source": [
    "ak.ravel(dR)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a63295e",
   "metadata": {},
   "source": [
    "Now we can plot that into a histogram:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "58146e6b",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = plt.hist(ak.ravel(dR), bins = 50, log = True)\n",
    "plt.xlabel(\"dR between good/offl. and TrigObj muon\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d017614a",
   "metadata": {},
   "source": [
    "Good. Now note that each offline muon might have several trig-objects matched to it, but in the end we only care about the closest one.\n",
    "To store the closes dR we can use `ak.min(dR, axis = -1)` where `axis=-1` tells the `min` function to search for the minimum `dR` at the per-event/object level."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7615ab9",
   "metadata": {},
   "outputs": [],
   "source": [
    "min_dR = ak.min(dR, axis = -1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69837334",
   "metadata": {},
   "source": [
    "If we compare the dR and `min_dR` we see the latter having one less layer of nesting:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6904c704",
   "metadata": {},
   "outputs": [],
   "source": [
    "ak.min(dR, axis = -1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "815bcacd",
   "metadata": {},
   "outputs": [],
   "source": [
    "dR"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "427c03fe",
   "metadata": {},
   "source": [
    "With this we can \"add\" the minimal dR to the muon trigger object to the good muon array:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "94d48809",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_muons[\"min_dR_trgObj\"] = min_dR"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9bb143e",
   "metadata": {},
   "source": [
    "Finally, we can now only keep the muons that pass the `IsoMu24` trigger:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "927722c1",
   "metadata": {},
   "outputs": [],
   "source": [
    "good_muons = good_muons[events.HLT_IsoMu24]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "940e2d6c",
   "metadata": {},
   "source": [
    "## Make tag and probe pairs, compute di-muon mass and filter Z-like pairs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4aa57b26",
   "metadata": {},
   "source": [
    "Here we will use the `ak.combinations` function: it takes e.g. the good muons arrays and builds combinations of N objects from it, see: (More also in the above tutorial)\n",
    "![](https://raw.githubusercontent.com/jpivarski-talks/2020-07-13-pyhep2020-tutorial/master/img/cartoon-combinations.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0960ed51",
   "metadata": {},
   "outputs": [],
   "source": [
    "zcands = ak.combinations(good_muons, 2, fields=[\"tag\", \"probe\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7721be1a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# filter out events that have no z candidates\n",
    "zcands = zcands[ak.num(zcands) > 0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21e55ea8",
   "metadata": {},
   "outputs": [],
   "source": [
    "zcands"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "deda489d",
   "metadata": {},
   "source": [
    "Now you can see that similar to the `ak.cartesian` above we get a new array with one more nesting layer -> the di-muon combinations!\n",
    "We need to apply the \"tag\" requirements to the \"tag\" leg of the combinations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f5cd29ba",
   "metadata": {},
   "outputs": [],
   "source": [
    "zcands = zcands[\n",
    "    # require the tag pt to be > 28\n",
    "    (zcands.tag.pt >= 28)\n",
    "    # require the tag to be matched to the trigger object\n",
    "    & (zcands.tag.min_dR_trgObj < 0.2)\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f1e9f6dd",
   "metadata": {},
   "source": [
    "Now that we have the proper tag-probe pairs we can further select those on the Z mass peak. \n",
    "For this we need to compute the mass as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16ef0391",
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute invariant mass\n",
    "mass = (zcands.tag + zcands.probe).mass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc08995a",
   "metadata": {},
   "outputs": [],
   "source": [
    "_ = plt.hist(ak.ravel(mass), bins = np.linspace(0,200,50), log = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f69c05de",
   "metadata": {},
   "source": [
    "### Task: select z candidates pairs that lie on the mass peak: 91±10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2cbdc01",
   "metadata": {},
   "outputs": [],
   "source": [
    "# solution:\n",
    "mass_Mz = (abs(mass - 91) <= 10)\n",
    "zcands = zcands[mass_Mz]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8bd22425",
   "metadata": {},
   "source": [
    "Finally, we can define a mask for probes being matched to the trigger object with `dR < 0.2`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a93278e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "goodprobe = zcands.probe.min_dR_trgObj < 0.2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a795649d",
   "metadata": {},
   "source": [
    "### Task: obtain the efficiency\n",
    "\n",
    "We define the trigger efficiency (vs the probe muon pt) as the ratio of trigger-object matched probes vs all selected probes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90f9f52e",
   "metadata": {},
   "source": [
    "#Hint\n",
    "\n",
    "Make a histogram of:\n",
    "* denominator: pt of all probes\n",
    "* numerator: pt of probes matched to a trigger object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d17c01f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = np.linspace(0,100,100)\n",
    "\n",
    "probe_pt = zcands.probe.pt\n",
    "\n",
    "denom_hist = plt.hist(ak.ravel(probe_pt), bins = bins, label = \"all probes\")\n",
    "num_hist = plt.hist(ak.ravel(probe_pt[goodprobe]), bins = bins, label = \"trg matched\")\n",
    "\n",
    "plt.legend()\n",
    "plt.xlabel(\"Probe pt\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "352f9d27",
   "metadata": {},
   "outputs": [],
   "source": [
    "hep.cms.label(\"Preliminary\", data=True, rlabel = \"2023 (13.6 TeV)\")\n",
    "plt.rcParams['figure.figsize'] = (8,6)\n",
    "\n",
    "plt.plot(num_hist[1][:-1], num_hist[0]/denom_hist[0], \".--\")\n",
    "\n",
    "plt.grid()\n",
    "plt.xlabel(\"Probe muon pT\")\n",
    "plt.ylabel(\"IsoMu24 Trigger efficiency\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3128584a",
   "metadata": {},
   "source": [
    "Congratulations! You have measured the trigger efficiency of the `IsoMu24` HLT path!\n",
    "\n",
    "Notice however that the statistics in the high-pt region is not that great.\n",
    "\n",
    "The reason for this is that given that the way `combinations` works is that it uses each pair always once.\n",
    "As the muons are pt-sorted this means that the tag pt is always larger than the probe pt.\n",
    "\n",
    "### Task: Compare the tag and the probe pt distributions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "086159da",
   "metadata": {},
   "outputs": [],
   "source": [
    "## solution\n",
    "bins = np.linspace(0,100,100)\n",
    "\n",
    "plt.hist(ak.ravel(zcands.tag.pt), bins = bins, label = \"tags\", histtype = \"step\")\n",
    "plt.hist(ak.ravel(zcands.probe.pt), bins = bins, label = \"probes\", histtype = \"step\")\n",
    "\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bff50e78",
   "metadata": {},
   "source": [
    "Note that we also the sub-leading muons might satisfy the tag requirements and this way we can \"probe\" the higher pt muon spectrum.\n",
    "\n",
    "In order to do that let's define a new tag muons array based on the good muons:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1abfcf76",
   "metadata": {},
   "outputs": [],
   "source": [
    "tag_muon_mask = good_muons.pt >= 28\n",
    "tag_muon_mask = tag_muon_mask & (good_muons.min_dR_trgObj < 0.2)\n",
    "\n",
    "tag_muons = good_muons[tag_muon_mask]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef4715ee",
   "metadata": {},
   "source": [
    "For probe muons we do not need to do anything -> using all good muons.\n",
    "\n",
    "Now instead of `ak.combinations` we will use the `ak.cartesian` (product) function to make combinations of the tag and probe muons:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e6ea0b81",
   "metadata": {},
   "outputs": [],
   "source": [
    "zcands2 = ak.cartesian({ \"tag\" : tag_muons, \"probe\" : good_muons})"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1fd91350",
   "metadata": {},
   "source": [
    "We need to reject combinations where the tag and probe muon are identical -> we can do this via the muon index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "565bc9e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "zcands2 = zcands2[zcands2.tag.idx!=zcands2.probe.idx]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12376273",
   "metadata": {},
   "source": [
    "### Task: compare the probe and tag pt distribution"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27178c0b",
   "metadata": {},
   "outputs": [],
   "source": [
    "## solution\n",
    "bins = np.linspace(0,100,100)\n",
    "\n",
    "plt.hist(ak.ravel(zcands2.tag.pt), bins = bins, label = \"tags\", histtype = \"step\")\n",
    "plt.hist(ak.ravel(zcands2.probe.pt), bins = bins, label = \"probes\", histtype = \"step\")\n",
    "\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d4bc261",
   "metadata": {},
   "source": [
    "Note that now the distributions are almost identical -> except that the tags do not go below 28 GeV as per our selection :)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5de94655",
   "metadata": {},
   "source": [
    "### Task: derive the efficiency curve using this alternate method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8e779d2",
   "metadata": {},
   "outputs": [],
   "source": [
    "bins = np.linspace(0,100,100)\n",
    "\n",
    "# compute invariant mass\n",
    "mass = (zcands2.tag + zcands2.probe).mass\n",
    "\n",
    "# restrict mass to Zpeak\n",
    "mass_Mz = (abs(mass - 91) <= 10)\n",
    "zcands2 = zcands2[mass_Mz]\n",
    "\n",
    "goodprobe2 = zcands2.probe.min_dR_trgObj < 0.2\n",
    "\n",
    "probe_pt = zcands2.probe.pt\n",
    "\n",
    "denom_hist = np.histogram(ak.ravel(probe_pt), bins = bins)\n",
    "num_hist = np.histogram(ak.ravel(probe_pt[goodprobe2]), bins = bins)\n",
    "\n",
    "plt.plot(num_hist[1][:-1], num_hist[0]/denom_hist[0], \".--\")\n",
    "\n",
    "\n",
    "plt.legend()\n",
    "plt.xlabel(\"Probe pt\")\n",
    "plt.grid()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aea482b5",
   "metadata": {},
   "source": [
    "### Task: compare the curves for the previous (combinations) and last method (cartesian)\n",
    "\n",
    "Hint: write a function to do the curve based on the zcands"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84a6f80a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_tnp_eff(zcands, bins = np.linspace(0,100,50)):\n",
    "    \n",
    "    # compute invariant mass\n",
    "    mass = (zcands.tag + zcands.probe).mass\n",
    "\n",
    "    # # # restrict mass to Zpeak\n",
    "    mass_Mz = (abs(mass - 91) <= 10)\n",
    "    zcands = zcands[mass_Mz]\n",
    "    \n",
    "    goodprobe = zcands.probe.min_dR_trgObj < 0.2\n",
    "\n",
    "    probe_pt = zcands.probe.pt\n",
    "\n",
    "    denom_hist = np.histogram(ak.ravel(probe_pt), bins = bins)\n",
    "    num_hist = np.histogram(ak.ravel(probe_pt[goodprobe]), bins = bins)\n",
    "\n",
    "    return num_hist[1][:-1], num_hist[0]/denom_hist[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2c54901",
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "x,y = make_tnp_eff(zcands)\n",
    "plt.plot(x,y, \"o-\", label = \"ak.comb: Tag > Probe Pt\")\n",
    "\n",
    "x,y = make_tnp_eff(zcands2)\n",
    "plt.plot(x,y, \"o-\", label = \"ak.cart: Any Probe pt\")\n",
    "\n",
    "plt.xlabel(\"Probe muon pt [GeV]\")\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e29e53a1",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
